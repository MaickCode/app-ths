import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  loginPage = "loginPage";
  public profileForm: FormGroup;
  constructor(public formBuilder: FormBuilder,
    public navCtrl: NavController) {
    this.profileForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellidos: ['', Validators.required],
      sexo: [''],
      correo: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

  }
  login = (form: any) => {
    if (this.profileForm.invalid) {
      alert("formulario invalida");
    }
    else {
      alert("formulario valido");
    }
  }
}

