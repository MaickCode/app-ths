import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage {
  
  public profileForm: FormGroup;
  constructor(public formBuilder: FormBuilder,
    public navCtrl: NavController) {
    this.profileForm = this.formBuilder.group({
      correo: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

  }
  login = (form: any) => {
    if (this.profileForm.invalid) {
      alert("formulario invalida");
    }
    else {
      alert("formulario valido");
    }
  }
}                
